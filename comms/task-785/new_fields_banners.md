# Nuevos Campos a los Banners de la Tienda

![Fields 1](https://gitlab.com/vmgabriel/img-public/-/raw/master/comms/task-785/2.png "store-title-description fields")
![Fields 2](https://gitlab.com/vmgabriel/img-public/-/raw/master/comms/task-785/3.png "products fields")

## Descripción

Se han agregado tres atributos:

- Titulo
- Descripción
- Productos

Dentro de estos campos habrá una serie de validaciones en el campo `productos`, si las tiendas no tienen dicho producto dentro de su inventario este generará un error, a su vez si se selecciona al menos un producto, estos se vuelven obligatorios:

- Titulo
- Descripción

Y generara error si no se llenan.

Además se han hecho cambios relacionados a un campo previo

- Tiendas

Este campo ahora permitirá seleccionar varias tiendas a un banner.
